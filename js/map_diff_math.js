am4core.ready(function() {

// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

 // Create map instance
var chart = am4core.create("div_map_diff_math", am4maps.MapChart);
chart.maxZoomLevel = 1;
// Set map definition
try {
    chart.geodata = am4geodata_worldHigh;
}
catch (e) {
    chart.raiseCriticalError(new Error("Map geodata could not be loaded. Please download the latest <a href=\"https://www.amcharts.com/download/download-v4/\">amcharts geodata</a> and extract its contents into the same directory as your amCharts files."));
}

// Set projection
chart.projection = new am4maps.projections.Mercator();

// Create map polygon series
// var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
// map polygon series (countries)
var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
polygonSeries.useGeodata = true;
// specify which countries to include

polygonSeries.exclude = ["AQ"];

// country area look and behavior
var polygonTemplate = polygonSeries.mapPolygons.template;
polygonTemplate.strokeOpacity = 1;
polygonTemplate.stroke = am4core.color("#ffffff");
polygonTemplate.fillOpacity = 0.9;
polygonTemplate.tooltipText = "{name}";


//Set min/max fill color for each area
polygonSeries.heatRules.push({
  property: "fill",
  target: polygonSeries.mapPolygons.template,
  min: chart.colors.getIndex(1).brighten(1),
  max: chart.colors.getIndex(1).brighten(-.7)
});

// Make map load polygon data (state shapes and names) from GeoJSON
polygonSeries.useGeodata = true;

// Set heatmap values for each state
polygonSeries.data = [
  {id: "MA",
  cnt: "Morocco",
  value: 62},
  {id: "NL",
  cnt: "Netherlands",
  value: 63},
  {id: "BA",
  cnt: "Bosnia and Herzegovina",
  value: 65},
  {id: "SK",
  cnt: "Slovak Republic",
  value: 65},
  {id: "SE",
  cnt: "Sweden",
  value: 65},
  {id: "BG",
  cnt: "Bulgaria",
  value: 67},
  {id: "PL",
  cnt: "Poland",
  value: 68},
  {id: "HR",
  cnt: "Croatia",
  value: 71},
  {id: "CZ",
  cnt: "Czech Republic",
  value: 71},
  {id: "GE",
  cnt: "Georgia",
  value: 74},
  {id: "ME",
  cnt: "Montenegro",
  value: 74},
  {id: "QA",
  cnt: "Qatar",
  value: 74},
  {id: "ES",
  cnt: "Spain",
  value: 76},
  {id: "DK",
  cnt: "Denmark",
  value: 77},
  {id: "FI",
  cnt: "Finland",
  value: 77},
  {id: "DE",
  cnt: "Germany",
  value: 77},
  {id: "RU",
  cnt: "Russian Federation",
  value: 77},
  {id: "AT",
  cnt: "Austria",
  value: 78},
  {id: "MT",
  cnt: "Malta",
  value: 78},
  {id: "FR",
  cnt: "France",
  value: 79},
  {id: "IT",
  cnt: "Italy",
  value: 79},
  {id: "LV",
  cnt: "Latvia",
  value: 79},
  {id: "IR",
  cnt: "Iran",
  value: 80},
  {id: "JP",
  cnt: "Japan",
  value: 80},
  {id: "XK",
  cnt: "Kosovo",
  value: 80},
  {id: "AM",
  cnt: "Armenia",
  value: 81},
  {id: "CL",
  cnt: "Chile",
  value: 81},
  {id: "HU",
  cnt: "Hungary",
  value: 81},
  {id: "CA",
  cnt: "Canada",
  value: 82},
  {id: "NZ",
  cnt: "New Zealand",
  value: 83},
  {id: "PK",
  cnt: "Pakistan",
  value: 83},
  {id: "RS",
  cnt: "Serbia",
  value: 83},
  {id: "AL",
  cnt: "Albania",
  value: 84},
  {id: "TW",
  cnt: "Chinese Taipei",
  value: 84},
  {id: "AE",
  cnt: "United Arab Emirates",
  value: 84},
  {id: "US",
  cnt: "United States",
  value: 85},
  {id: "KZ",
  cnt: "Kazakhstan",
  value: 86},
  {id: "IE",
  cnt: "Ireland",
  value: 88},
  {id: "GB",
  cnt: "England",
  value: 88},
  {id: "AU",
  cnt: "Australia",
  value: 89},
  {id: "BH",
  cnt: "Bahrain",
  value: 89},
  {id: "CY",
  cnt: "Cyprus",
  value: 89},
  {id: "HK",
  cnt: "Hong Kong SAR",
  value: 89},
  {id: "LT",
  cnt: "Lithuania",
  value: 89},
  {id: "SA",
  cnt: "Saudi Arabia",
  value: 89},
  {id: "KW",
  cnt: "Kuwait",
  value: 90},
  {id: "MK",
  cnt: "North Macedonia",
  value: 90},
  {id: "OM",
  cnt: "Oman",
  value: 92},
  {id: "PH",
  cnt: "Philippines",
  value: 93},
  {id: "SG",
  cnt: "Singapore",
  value: 93},
  {id: "XI",
  cnt: "Northern Ireland",
  value: 94},
  {id: "AZ",
  cnt: "Azerbaijan",
  value: 97},
  {id: "PT",
  cnt: "Portugal",
  value: 97},
];

// Set up heat legend
let heatLegend = chart.createChild(am4maps.HeatLegend);
heatLegend.series = polygonSeries;
heatLegend.align = "right";
heatLegend.valign = "bottom";
heatLegend.width = am4core.percent(20);
heatLegend.valueAxis.renderer.labels.template.fontSize = 14;
heatLegend.marginRight = am4core.percent(35);
heatLegend.minValue = 0;
heatLegend.maxValue = 100;

// Set up custom heat map legend labels using axis ranges
var minRange = heatLegend.valueAxis.axisRanges.create();
minRange.value = heatLegend.minValue;
minRange.label.text = "0";
var maxRange = heatLegend.valueAxis.axisRanges.create();
maxRange.value = heatLegend.maxValue;
maxRange.label.text = "100%";

// Blank out internal heat legend value axis labels
heatLegend.valueAxis.renderer.labels.template.adapter.add("text", function(labelText) {
  return "";
});

// Configure series tooltip
var polygonTemplate = polygonSeries.mapPolygons.template;
polygonTemplate.tooltipText = "[bold]{cnt}[\] \n All math (17 topics): {value}%";
polygonTemplate.nonScalingStroke = true;
polygonTemplate.strokeWidth = 0.5;

// Create hover state and set alternative fill color
var hs = polygonTemplate.states.create("hover");
hs.properties.fill = am4core.color("#3c5bdc");

}); // end am4core.ready()
