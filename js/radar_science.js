am4core.ready(function() {

// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end


var temperatures = {
    "EUROPE": [
      ["Belgium",94,94,94],
      ["Spain",35,35,35],
      ["Portugal",29,29,29],
      ["Croatia",7,7,7],
      ["Serbia",2,2,2],
      ["Austria",1,1,1],
      ["Montenegro",0,0,0],
      ["Sweden",0,0,0],
      ["Denmark",-1,-1,-1],
      ["Bosnia and Herzegovina",-1,-1,-1],
      ["Italy",-2,-2,-2],
      ["Finland",-4,-4,-4],
      ["Norway",-10,-10,-10],
      ["Slovak Republic",-11,-11,-11],
      ["Lithuania",-16,-16,-16],
      ["Kosovo",-16,-16,-16],
      ["North Macedonia",-17,-17,-17],
      ["Georgia",-18,-18,-18],
      ["Germany",-19,-19,-19],
      ["Hungary",-19,-19,-19],
      ["Poland",-20,-20,-20],
      ["Albania",-21,-21,-21],
      ["Armenia",-21,-21,-21],
      ["Azerbaijan",-22,-22,-22],
      ["Latvia",-22,-22,-22],
      ["Czech Republic",-24,-24,-24],
      ["Malta",-24,-24,-24],
      ["Cyprus",-24,-24,-24],
      ["Russian Federation",-27,-27,-27],
      ["France",-28,-28,-28],
      ["Northern Ireland",-37,-37,-37],
      ["Bulgaria",-38,-38,-38],
      ["Netherlands",-40,-40,-40],
      ["Ireland",-41,-41,-41],
    ],

    "ASIA": [
      ["Philippines",83,83,83],
      ["Pakistan",64,64,64],
      ["Oman",45,45,45],
      ["United Arab Emirates",44,44,44],
      ["Bahrain",43,43,43],
      ["Turkey",34,34,34],
      ["Qatar",26,26,26],
      ["Japan",17,17,17],
      ["Chinese Taipei",11,11,11],
      ["Singapore",9,9,9],
      ["Iran",3,3,3],
      ["Kuwait",3,3,3],
      ["Korea",-4,-4,-4],
      ["Saudi Arabia",-9,-9,-9],
      ["Kazakhstan",-35,-35,-35],


    ],
    "OTHER": [
      ["South Africa",70,70,70],
      ["Chile",22,22,22],
      ["United States",8,8,8],
      ["Canada",5,5,5],
      ["Morocco",-21,-21,-21],
      ["Australia",-22,-22,-22],
      ["New Zealand",-35,-35,-35],



    ]
}

var startYear = 2019;
var endYear = 2019;
var currentYear = 2019;
var colorSet = new am4core.ColorSet();

var chart = am4core.create("chartdiv_radar_science", am4charts.RadarChart);
chart.numberFormatter.numberFormat = "+#.0|#.0|0.0";
chart.hiddenState.properties.opacity = 0;

chart.startAngle = 270 - 180;
chart.endAngle = 270 + 180;

chart.padding(5,15,5,10)
chart.radius = am4core.percent(65);
chart.innerRadius = am4core.percent(40);

// year label goes in the middle
var yearLabel = chart.radarContainer.createChild(am4core.Label);
yearLabel.horizontalCenter = "middle";
yearLabel.verticalCenter = "middle";
yearLabel.fill = am4core.color("#673AB7");
yearLabel.fontSize = 30;
yearLabel.text = 'Science';

// zoomout button
var zoomOutButton = chart.zoomOutButton;
zoomOutButton.dx = 0;
zoomOutButton.dy = 0;
zoomOutButton.marginBottom = 15;
zoomOutButton.parent = chart.rightAxesContainer;

// scrollbar
chart.scrollbarX = new am4core.Scrollbar();
chart.scrollbarX.parent = chart.rightAxesContainer;
chart.scrollbarX.orientation = "vertical";
chart.scrollbarX.align = "center";
chart.scrollbarX.exportable = false;

// vertical orientation for zoom out button and scrollbar to be positioned properly
chart.rightAxesContainer.layout = "vertical";
chart.rightAxesContainer.padding(120, 20, 120, 20);

// category axis
var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.renderer.grid.template.location = 0;
categoryAxis.dataFields.category = "country";

var range = categoryAxis.axisRanges.create();
range.category = "Poland";
range.axisFill.fill = chart.colors.getIndex(1);
range.axisFill.fillOpacity = 0.2;
range.label.text = ""; // end am4core.ready()

var categoryAxisRenderer = categoryAxis.renderer;
var categoryAxisLabel = categoryAxisRenderer.labels.template;
categoryAxisLabel.location = 0.5;
categoryAxisLabel.radius = 28;
categoryAxisLabel.relativeRotation = 90;

categoryAxisRenderer.fontSize = 11;
categoryAxisRenderer.minGridDistance = 10;
categoryAxisRenderer.grid.template.radius = -25;
categoryAxisRenderer.grid.template.strokeOpacity = 0.05;
categoryAxisRenderer.grid.template.interactionsEnabled = false;

categoryAxisRenderer.ticks.template.disabled = true;
categoryAxisRenderer.axisFills.template.disabled = true;
categoryAxisRenderer.line.disabled = true;

categoryAxisRenderer.tooltipLocation = 0.5;
categoryAxis.tooltip.defaultState.properties.opacity = 0;

// value axis
var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
valueAxis.min = -70;
valueAxis.max = 100;
valueAxis.strictMinMax = true;
valueAxis.tooltip.defaultState.properties.opacity = 0;
valueAxis.tooltip.animationDuration = 0;
valueAxis.cursorTooltipEnabled = true;
valueAxis.zIndex = 10;

var valueAxisRenderer = valueAxis.renderer;
valueAxisRenderer.axisFills.template.disabled = true;
valueAxisRenderer.ticks.template.disabled = true;
valueAxisRenderer.minGridDistance = 20;
valueAxisRenderer.grid.template.strokeOpacity = 0.05;


// series
var series = chart.series.push(new am4charts.RadarColumnSeries());
series.columns.template.width = am4core.percent(90);
series.columns.template.strokeOpacity = 0;
series.dataFields.valueY = "value" + currentYear;
series.dataFields.categoryX = "country";
series.tooltipText = "{categoryX}:{valueY.value}";

// this makes columns to be of a different color, depending on value
series.heatRules.push({ target: series.columns.template, property: "fill", minValue: 0, maxValue: 6, min: am4core.color("#673AB7"), max: am4core.color("#F44336"), dataField: "valueY" });

// cursor
var cursor = new am4charts.RadarCursor();
chart.cursor = cursor;
cursor.behavior = "zoomX";

cursor.xAxis = categoryAxis;
cursor.innerRadius = am4core.percent(40);
cursor.lineY.disabled = true;

cursor.lineX.fillOpacity = 0.2;
cursor.lineX.fill = am4core.color("#000000");
cursor.lineX.strokeOpacity = 0;
cursor.fullWidthLineX = true;

// year slider
// var yearSliderContainer = chart.createChild(am4core.Container);
// yearSliderContainer.layout = "vertical";
// yearSliderContainer.padding(0, 38, 0, 38);
// yearSliderContainer.width = am4core.percent(100);
//
// var yearSlider = yearSliderContainer.createChild(am4core.Slider);
// yearSlider.events.on("rangechanged", function () {
//     updateRadarData(startYear + Math.round(yearSlider.start * (endYear - startYear)));
// })
// yearSlider.orientation = "horizontal";
// yearSlider.start = 0.5;
// yearSlider.exportable = false;

chart.data = generateRadarData();

function generateRadarData() {
    var data = [];
    var i = 0;
    for (var continent in temperatures) {
        var continentData = temperatures[continent];

        continentData.forEach(function (country) {
            var rawDataItem = { "country": country[0] }

            for (var y = 2; y < country.length; y++) {
                rawDataItem["value" + (startYear + y - 2)] = country[y];
            }

            data.push(rawDataItem);
        });

        createRange(continent, continentData, i);
        i++;

    }
    return data;
}


function updateRadarData(year) {
    if (currentYear != year) {
        currentYear = year;
        yearLabel.text = String(currentYear);
        series.dataFields.valueY = "value" + currentYear;
        chart.invalidateRawData();
    }
}

function createRange(name, continentData, index) {

    var axisRange = categoryAxis.axisRanges.create();
    axisRange.axisFill.interactionsEnabled = true;
    axisRange.text = name;
    // first country
    axisRange.category = continentData[0][0];
    // last country
    axisRange.endCategory = continentData[continentData.length - 1][0];

    // every 3rd color for a bigger contrast
    axisRange.axisFill.fill = colorSet.getIndex(index * 3);
    axisRange.grid.disabled = true;
    axisRange.label.interactionsEnabled = false;
    axisRange.label.bent = true;

    var axisFill = axisRange.axisFill;
    axisFill.innerRadius = -0.001; // almost the same as 100%, we set it in pixels as later we animate this property to some pixel value
    axisFill.radius = -20; // negative radius means it is calculated from max radius
    axisFill.disabled = false; // as regular fills are disabled, we need to enable this one
    axisFill.fillOpacity = 1;
    axisFill.togglable = true;

    axisFill.showSystemTooltip = true;
    axisFill.readerTitle = "click to zoom";
    axisFill.cursorOverStyle = am4core.MouseCursorStyle.pointer;

    axisFill.events.on("hit", function (event) {
        var dataItem = event.target.dataItem;
        if (!event.target.isActive) {
            categoryAxis.zoom({ start: 0, end: 1 });
        }
        else {
            categoryAxis.zoomToCategories(dataItem.category, dataItem.endCategory);
        }
    })

    // hover state
    var hoverState = axisFill.states.create("hover");
    hoverState.properties.innerRadius = -10;
    hoverState.properties.radius = -25;

    var axisLabel = axisRange.label;
    axisLabel.location = 0.5;
    axisLabel.fill = am4core.color("#ffffff");
    axisLabel.radius = 3;
    axisLabel.relativeRotation = 0;
}

// var slider = yearSliderContainer.createChild(am4core.Slider);
// slider.start = 1;
// slider.exportable = false;
// slider.events.on("rangechanged", function () {
//     var start = slider.start;
//
//     chart.startAngle = 270 - start * 179 - 1;
//     chart.endAngle = 270 + start * 179 + 1;
//
//     valueAxis.renderer.axisAngle = chart.startAngle;
// })

}); // end am4core.ready()
